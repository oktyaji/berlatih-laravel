@extends('layout.film')

@section('content')
    <a href="/cast/create" class="btn btn-primary mb-3">+ Tambah</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur (Tahun)</th>
            <th scope="col">Bio</th>
            <th scope="col">Aksi</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $value)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>
                    <td>
                        <form action="/cast/{{ $value->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <a href="/cast/{{$value->id}}" class="btn btn-info">detail</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning">edit</a>
                            <button type="submit" class="btn btn-danger">hapus</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td>Data masih kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
