@extends('layout.film')

@section('content')

    <h3>Halaman Detail {{ $cast->nama }}</h3>
    <hr>
    <p>{{ $cast->nama }} ({{ $cast->umur }} Tahun)</p>
    <p>{{ $cast->bio }}</p>

@endsection
