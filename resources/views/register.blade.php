<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <div class="form-group">
            <label for="firstName">First Name:</label><br>
            <input type="text" name="firstName" id="firstName">
        </div><br>
        <div class="form-group">
            <label for="lastName">Last Name:</label><br>
            <input type="text" name="lastName" id="lastName">
        </div><br>
        <div class="from-goup">
            <Label for="gender">Gender:</Label><br>
            <input type="radio" name="male" id="male" value="male"><label for="male">Male </label><br>
            <input type="radio" name="famele" id="famele" value="famale"><label for="famele">Famele </label><br>
            <input type="radio" name="other" id="other" value="other"><label for="other">Other</label><br>
        </div><br>
        <div class="form-group">
            <label for="nationality">Nationality</label><br>
            <select id="nationality" name="nationality" id="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="amerika">Amerika</option>
                <option value="inggris">Inggris</option>
            </select>
        </div><br>
        <div class="form-group">
            <label for="language">Language Spoken</label><br>
            <input type="checkbox" name="indonesia" value='indonesia' id="language"> Bahasa Indonesia <br>
            <input type="checkbox" name="english" value='english' id="language"> English <br>
            <input type="checkbox" name="other" value='other' id="language"> Other <br>
        </div><br>
        <div class="form-group">
            <label for="bio">Bio: </label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        </div>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
