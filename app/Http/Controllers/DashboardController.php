<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function table() {
        return view('admin.table');
    }

    public function datatables() {
        return view('admin.data-tables');
    }
}
