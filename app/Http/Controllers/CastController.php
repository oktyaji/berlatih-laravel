<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index() {
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }

    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // Query Builder
        // DB::table('cast')->insert([
        //     'nama' => $request->nama,
        //     'umur' => $request->umur,
        //     'bio' => $request->bio
        // ]);

        // ORM
        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();

        return redirect('/cast');
    }

    public function show($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::findOrFail($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::findOrFail($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // Query Builder
        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request['nama'],
        //         'umur' => $request['umur'],
        //         'bio' => $request['bio']
        // ]);

        // ORM v.1
        // $cast = Cast::find($id);
        // $cast->nama = $request['nama'];
        // $cast->umur = $request['umur'];
        // $cast->bio = $request['bio'];
        // $cast->save();

        // ORM v.2
        $update = Cast::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function destroy($id) {
        // Query Builder
        // DB::table('cast')->where('id', $id)->delete();

        // ORM
        Cast::destroy($id);

        return redirect('/cast');
    }
}
